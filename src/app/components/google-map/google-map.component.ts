import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { MapPolygon } from '@angular/google-maps';
import { Local, LOCATIONS } from '../../services/location'

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GoogleMapComponent {

  locations: Local[] = LOCATIONS;

  apiLoaded: Observable<boolean>;

  options: google.maps.MapOptions = {
    center: { lat: 14.409168, lng: 100.585400 },
    zoom: 16,
    mapTypeId: 'satellite',
    fullscreenControl: false,
    mapTypeControl: false,
    draggable: false,
    streetViewControl: false
  };

  coords_P1: google.maps.LatLngLiteral[] = [
    { lat: 14.409978, lng: 100.580351 },
    { lat: 14.410132, lng: 100.581483 },
    { lat: 14.407641, lng: 100.581602 },
    { lat: 14.408754, lng: 100.580388 },
  ];

  coords_P2: google.maps.LatLngLiteral[] = [
    { lat: 14.410115, lng: 100.581533 },
    { lat: 14.410239, lng: 100.583852 },
    { lat: 14.409764, lng: 100.583882 },
    { lat: 14.409689, lng: 100.583301 },
    { lat: 14.409320, lng: 100.583300 },
    { lat: 14.409207, lng: 100.582199 },
    { lat: 14.408762, lng: 100.582197 },
    { lat: 14.408729, lng: 100.581622 },
    { lat: 14.410141, lng: 100.581486 },
  ];

  coords_P3: google.maps.LatLngLiteral[] = [
    { lat: 14.409211, lng: 100.582237 },
    { lat: 14.409300, lng: 100.583280 },
    { lat: 14.408223, lng: 100.583435 },
    { lat: 14.408148, lng: 100.582308 },
  ];

  coords_P4: google.maps.LatLngLiteral[] = [
    { lat: 14.409394, lng: 100.583414 },
    { lat: 14.409444, lng: 100.584010 },
    { lat: 14.409127, lng: 100.584034 },
    { lat: 14.409094, lng: 100.583439 },
  ];

  coords_P5: google.maps.LatLngLiteral[] = [
    { lat: 14.409037, lng: 100.583439 },
    { lat: 14.409089, lng: 100.584044 },
    { lat: 14.408841, lng: 100.584054 },
    { lat: 14.408860, lng: 100.583473 },
  ];

  coords_P6: google.maps.LatLngLiteral[] = [
    { lat: 14.409687, lng: 100.583379 },
    { lat: 14.409739, lng: 100.583916 },
    { lat: 14.409544, lng: 100.583936 },
    { lat: 14.409495, lng: 100.583434 },
  ];
  coords_P7: google.maps.LatLngLiteral[] = [
    { lat: 14.409730, lng: 100.583325 },
    { lat: 14.409788, lng: 100.583985 },
    { lat: 14.410082, lng: 100.583975 },
    { lat: 14.410160, lng: 100.585115 },
    { lat: 14.407605, lng: 100.585331 },
    { lat: 14.407548, lng: 100.584163 },
    { lat: 14.408879, lng: 100.584044 },
    { lat: 14.408860, lng: 100.583419 },
  ];
  coords_P8: google.maps.LatLngLiteral[] = [
    { lat: 14.410786, lng: 100.584853 },
    { lat: 14.410906, lng: 100.586275 },
    { lat: 14.410354, lng: 100.586322 },
    { lat: 14.410242, lng: 100.584922 },
  ];
  coords_P9: google.maps.LatLngLiteral[] = [
    { lat: 14.409289, lng: 100.585428 },
    { lat: 14.409299, lng: 100.586004 },
    { lat: 14.409043, lng: 100.586042 },
    { lat: 14.408992, lng: 100.585456 },
  ];
  coords_P10: google.maps.LatLngLiteral[] = [
    { lat: 14.408992, lng: 100.585456 },
    { lat: 14.409019, lng: 100.586030 },
    { lat: 14.408743, lng: 100.586072 },
    { lat: 14.408714, lng: 100.585453 },
  ];
  coords_P11: google.maps.LatLngLiteral[] = [
    { lat: 14.410178, lng: 100.585331 },
    { lat: 14.410265, lng: 100.586624 },
    { lat: 14.408822, lng: 100.586769 },
    { lat: 14.408773, lng: 100.586043 },
    { lat: 14.409328, lng: 100.586001 },
    { lat: 14.409291, lng: 100.585401 },
  ];
  coords_P12: google.maps.LatLngLiteral[] = [
    { lat: 14.408798, lng: 100.589524 },
    { lat: 14.409726, lng: 100.590202 },
    { lat: 14.409970, lng: 100.589832 },
    { lat: 14.409022, lng: 100.589061 },
  ];
  coords_P13: google.maps.LatLngLiteral[] = [
    { lat: 14.408790, lng: 100.588342 },
    { lat: 14.410367, lng: 100.589678 },
    { lat: 14.410118, lng: 100.590493 },
    { lat: 14.408357, lng: 100.589248 },
  ];
  coords_P14: google.maps.LatLngLiteral[] = [
    { lat: 14.408913, lng: 100.588563 },
    { lat: 14.410304, lng: 100.589757 },
    { lat: 14.410203, lng: 100.590021 },
    { lat: 14.408662, lng: 100.588864 },
  ];

  polygon_Option: google.maps.PolygonOptions[] = [
    {
      fillColor: "#33b2df",
      fillOpacity: 0.8,
      paths: this.coords_P1,
      zIndex: 1
    },
    {
      fillColor: "#546E7A",
      fillOpacity: 0.8,
      paths: this.coords_P2,
      zIndex: 1
    },
    {
      fillColor: "#d4526e",
      fillOpacity: 0.8,
      paths: this.coords_P3,
      zIndex: 1
    },
    {
      fillColor:  "#13d8aa",
      fillOpacity: 0.8,
      paths: this.coords_P4,
      zIndex: 2
    },
    {
      fillColor: "#A5978B",
      fillOpacity: 0.8,
      paths: this.coords_P5,
      zIndex: 2
    },
    {
      fillColor: "#2b908f",
      fillOpacity: 0.8,
      paths: this.coords_P6,
      zIndex: 2
    },
    {
      fillColor: "#f9a3a4",
      fillOpacity: 0.2,
      paths: this.coords_P7,
      zIndex: 1
    },
    {
      fillColor: "#90ee7e",
      fillOpacity: 0.8,
      paths: this.coords_P8,
      zIndex: 1
    },
    {
      fillColor: "#f48024",
      fillOpacity: 0.8,
      paths: this.coords_P9,
      zIndex: 2
    },
    {
      fillColor: "#69d2e7",
      fillOpacity: 0.8,
      paths: this.coords_P10,
      zIndex: 2
    },
    {
      fillColor: "#666633",
      fillOpacity: 0.8,
      paths: this.coords_P11,
      zIndex: 1
    },
    {
      fillColor: "##f00c0c",
      fillOpacity: 0.4,
      paths: this.coords_P12,
      zIndex: 2
    },
    {
      fillColor: "#6600cc",
      fillOpacity: 0.8,
      paths: this.coords_P13,
      zIndex: 1
    },
    {
      fillColor: "#f0ec0c",
      fillOpacity: 0.8,
      paths: this.coords_P14,
      zIndex: 2
    },
  ];

  constructor(httpClient: HttpClient) {
    this.apiLoaded = httpClient.jsonp('https://maps.googleapis.com/maps/api/js?key=AIzaSyDLWMGJNRhSe2diiYBHd_CUVa66xfbn1GA',
                      'callback')
        .pipe(
          map(() => true),
          catchError(() => of(false)),
        );
  }

  clickLocation(event: google.maps.MouseEvent) {
    // this.options.center = (event.latLng.toJSON());
  }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
