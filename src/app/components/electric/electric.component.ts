import { Component, ViewChild, OnInit } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import {Electric, Local, locationList, LOCATIONS, slaveList } from '../../services/location';
import { LocationService } from '../../services/location.service';

import {
  ApexNonAxisChartSeries,
  ApexAxisChartSeries,
  ApexTitleSubtitle,
  ApexResponsive,
  ApexChart,
  ApexFill,
  ApexDataLabels,
  ApexPlotOptions,
  ApexLegend,
  ApexXAxis,
  ApexYAxis,
  ApexStroke,
  ApexGrid,
  ApexTheme,
} from "ng-apexcharts";
import * as moment from 'moment';
import { QueryService } from 'src/app/services/query.service';
import { InfluxdbService } from 'src/app/services/influxdb.service';

export type ChartOptions = {
  series: ApexNonAxisChartSeries | ApexAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  fill: ApexFill;
  legend: ApexLegend;
  dataLabels: ApexDataLabels;
  colors: string[];
  plotOptions: ApexPlotOptions;
  title: ApexTitleSubtitle;
  x_axis: ApexXAxis;
  y_axis: ApexYAxis;
  grid: ApexGrid;
  stroke: ApexStroke;
  theme: ApexTheme;
};

@Component({
  selector: 'app-electric',
  templateUrl: './electric.component.html',
  styleUrls: [
    '../../../../node_modules/primeng/resources/themes/saga-blue/theme.css',
    '../../../../node_modules/primeng/resources/primeng.min.css',
    '../../../../node_modules/primeicons/primeicons.css',
    './electric.component.scss']
})
export class ElectricComponent implements OnInit {

  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartsOptions: any[];

  getVoltage3PPromise = (location: string, slave: number, date:Date): Promise<number[]> => {
    return new Promise<number[]>((resolve,reject) => {
      try{
          let c = this.selectedDate;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          // const q = this.query.ParamStringMean("tfer", "tfer_pm",
          //                 "60m", s, e,
          //                 location, slave.toString(),
          //                 ["cA"],
          //                 "mean", "true");
         const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "vAB" or r["_field"] == "vBC" or r["_field"] == "vCA")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [380, 381, 382];
              if (data.length > 0) {
                data.forEach(element => {
                  if(element._field === 'vAB')
                    d[0]=(element._value);
                  else if (element._field === 'vBC')
                    d[1]=(element._value);
                  else if (element._field === 'vCA')
                    d[2]=(element._value);
                });
              }
              if(data.length === 0)
              {
                for (let index = 0; index < 3; index++) {
                  d[index]=(0);
                }
              }
              resolve(d);
            });

      }
      catch(e)
      {
        console.log(e);
        let d = [];
        for (let index = 0; index < 24; index++) {
          d.push(0);
        }
        reject(d);
      }
    });
  };

  getVoltage2PPromise = (location: string, slave: number, date:Date): Promise<number[]> => {
    return new Promise<number[]>((resolve,reject) => {
      try{
          let c = this.selectedDate;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          // const q = this.query.ParamStringMean("tfer", "tfer_pm",
          //                 "60m", s, e,
          //                 location, slave.toString(),
          //                 ["cA"],
          //                 "mean", "true");
         const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "vAN" or r["_field"] == "vBN" or r["_field"] == "vCN")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data => {
            let d = [220, 221, 222];
            if (data.length > 0) {
              data.forEach(element => {
                if(element._field === 'vAN')
                  d[0]=(element._value);
                else if (element._field === 'vBN')
                  d[1]=(element._value);
                else if (element._field === 'vCN')
                  d[2]=(element._value);
              });
            }
            if(data.length === 0)
            {
              for (let index = 0; index < 3; index++) {
                d[index]=(0);
              }
            }
            resolve(d);
          });

      }
      catch(e)
      {
        console.log(e);
        let d = [];
        for (let index = 0; index < 24; index++) {
          d.push(0);
        }
        reject(d);
      }
    });
  };

  getAmperePromise = (location: string, slave: number, date:Date): Promise<number[]> => {
    return new Promise<number[]>((resolve,reject) => {
      try{
          let c = this.selectedDate;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          // const q = this.query.ParamStringMean("tfer", "tfer_pm",
          //                 "60m", s, e,
          //                 location, slave.toString(),
          //                 ["cA"],
          //                 "mean", "true");
         const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "cA" or r["_field"] == "cB" or r["_field"] == "cC")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [1, 1, 1];
              if (data.length > 0) {
                data.forEach(element => {
                  if(element._field === 'cA')
                    d[0]=(element._value);
                  else if (element._field === 'cB')
                    d[1]=(element._value);
                  else if (element._field === 'cC')
                    d[2]=(element._value);
                });
              }
              if(data.length === 0)
              {
                for (let index = 0; index < 3; index++) {
                  d[index]=(0);
                }
              }
              resolve(d);
            });

      }
      catch(e)
      {
        console.log(e);
        let d = [];
        for (let index = 0; index < 24; index++) {
          d.push(0);
        }
        reject(d);
      }
    });
  };

  allPromise(id, date) : any {
    const a = Promise.all<number[]>( [
      this.getVoltage3PPromise(locationList[id], slaveList[id], date),
      this.getVoltage2PPromise(locationList[id], slaveList[id], date),
      this.getAmperePromise(locationList[id], slaveList[id], date),
      // this.getPromise("P1", 1),  // จุดที่ 1
      // this.getPromise("P2", 1),  // จุดที่ 2
      // this.getPromise("P3", 1),  // จุดที่ 3
      // this.getPromise("P5", 1),  // จุดที่ 4
      // this.getPromise("P5", 2),  // จุดที่ 5
      // this.getPromise("P4", 1),  // จุดที่ 6
      // this.getPromise("P4", 2),  // จุดที่ 7
      // this.getPromise("P6", 1),  // จุดที่ 8
      // this.getPromise("P7", 1),  // จุดที่ 9
      // this.getPromise("P7", 2),  // จุดที่ 10
      // this.getPromise("P7", 3),  // จุดที่ 11
      // this.getPromise("P8", 1),  // จุดที่ 12
      // this.getPromise("P8", 2),  // จุดที่ 13
      // this.getPromise("P8", 3),  // จุดที่ 14
    ] );
    return a;
  };

  electric: Electric[] = [
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
    { P3: [380, 381, 382], P2: [220, 221, 222], A: [20, 21, 22] },
  ];
  dateChanged = (date) => {
    this.selectedDate = date;
    this.fetchData(date);
  }

  private influx: InfluxdbService;
  private query: QueryService;

  constructor(private localService: LocationService) {
    this.query = new QueryService();
    this.influx = new InfluxdbService();
    this.localService.date.subscribe(this.dateChanged);
    this.chartsOptions = [];
    for (let i = 0; i < LOCATIONS.length; i++)
    {
      this.chartsOptions.push({
        local: LOCATIONS[i],
        series: [
          {
            name: "kW",
            data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
          },
          {
            name: "kvar",
            data: [71, 1, 5, 15, 94, 26, 99, 19, 48]
          },
          {
            name: "kVA",
            data: [100, 14, 53, 5, 9, 92, 9, 61, 84]
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight"
        },
        title: {
          text: LOCATIONS[i].name + ' Consumption',
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
            opacity: 0.5
          }
        },
        x_axis: {
          categories: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep"
          ]
        }
      });
    }
  }

  ngOnInit(): void {
  }

  fetchData(date): void {
    for (let i = 0; i < LOCATIONS.length; i++)
    {
      this.allPromise(i,date).then((result) =>
      {
        console.log(result);
        this.electric[i].P2 = result[0];
        this.electric[i].P3 = result[1];
        this.electric[i].A = result[2];
        // this.chartsOptions[i].series = result[i];
      });
    }
  }

  selectedDate: Date = new Date();
  onDateChange(event) {
    console.log(event);
    this.selectedDate= event;
    this.fetchData(event);
  }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
