import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { LocationService } from 'src/app/services/location.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { EXAMPLE_DATA, SystemTableDataSource, SystemTableItem } from './system-table-datasource';

@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.scss']
})
export class SystemComponent implements AfterViewInit, OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<SystemTableItem>;
  dataSource: SystemTableDataSource;
  data: SystemTableItem[] = [];
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['location', 'connection', 'data'];

  isLoadingResults = true;
  isRateLimitReached = false;
  selectedDate: Date;

  dateChanged = (date) => {
    this.selectedDate = date;
    this.ngAfterViewInit();
  }

  constructor(private localService: LocationService) {
    this.localService.date.subscribe(this.dateChanged);
  }


  ngOnInit(): void {
  }

  ngAfterViewInit() {
    let d: SystemTableItem[] = [];
    this.dataSource = new SystemTableDataSource(this.selectedDate);
    this.isLoadingResults = true;
    this.dataSource.allSystemPromise(this.selectedDate).then((result) => {
      for (const key in result) {
        if (Object.prototype.hasOwnProperty.call(result, key)) {
          const element = result[key];
          if (result[key].length == 0) {
            d.push(EXAMPLE_DATA[key]);
          } else {
            d.push(result[key][0]);
          }
        }
      }
    }).then(()=>{
      this.data = d;
    }).finally(() => {
      this.isLoadingResults = false;
      this.isRateLimitReached = false;
    });
  }

  onDateChange(event) {
    console.log(event);
    this.selectedDate= event;
    this.ngAfterViewInit();
  }


}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
