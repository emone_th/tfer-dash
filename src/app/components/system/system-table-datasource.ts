import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { Local, LOCATIONS } from '../../services/location'
import { LocationService } from '../../services/location.service'
import { QueryService } from 'src/app/services/query.service';
import { InfluxdbService } from 'src/app/services/influxdb.service';
import * as moment from 'moment';
// TODO: Replace this with your own data model type
export interface SystemTableItem {
  data: string;
  location: Local;
  connection: boolean;
}

// TODO: replace this with real data from your application
export const EXAMPLE_DATA: SystemTableItem[] = [
  {location: LOCATIONS[0], connection: true, data: '00E841EA4EB04131E0AE41185AC240AFCAC0FF0000C2411183'},
  {location: LOCATIONS[1], connection: true, data: '02C7437FD0C9433D12C74331C3C8434F37664357816743DE9267436A8000800080'},
  {location: LOCATIONS[2], connection: true, data: '015A41FE1FC64178B83341E350C64178B8'},
  {location: LOCATIONS[3], connection: true, data: '0393407DAC634053BB5040C8E23641C6FD'},
  {location: LOCATIONS[4], connection: true, data: '050100F40B0C00B940794226406C5FB3409E37B3421ACB1400190926154871'},
  {location: LOCATIONS[5], connection: true, data: '04743FD378673F15A4613F5FF26A3FB21F'},
  {location: LOCATIONS[6], connection: true, data: '02C843AE0DC843AD3AC6439DB7C743A8AA6543BC266743D4BC6643EFC800800080'},
  {location: LOCATIONS[7], connection: true, data: '02C743D8DCC843470EC6432F7DC7431B78644317EA674346866643F38C00800080'},
  {location: LOCATIONS[8], connection: true, data: 'Fl0382407AF98B3FAF9C093FAF79B740DC0'},
  {location: LOCATIONS[9], connection: true, data: '019241CDBA82400524644181639241CDBA'},
  {location: LOCATIONS[10], connection: true, data: '03984028D96E40FB34594064203E41EC41'},
  {location: LOCATIONS[11], connection: true, data: '02CB435D64CB43332AC943F380CA43D7AF6943098B6B436E386943125F00800080'},
  {location: LOCATIONS[12], connection: true, data: '050100F40B0C00FC41D99C94413CE1FC41C9991E43D06C14001B08310EC0DA'},
  {location: LOCATIONS[13], connection: false, data: '04533F0E104E3FA1B74E3F611A4F3FF5F5'},
];

/**
 * Data source for the SystemTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class SystemTableDataSource //extends DataSource<SystemTableItem>
{
  private influx: InfluxdbService;
  private query: QueryService;

  constructor(date: Date) {
    // super();
    this.query = new QueryService();
    this.influx = new InfluxdbService();
  }

  // /**
  //  * Connect this data source to the table. The table will only update when
  //  * the returned stream emits new items.
  //  * @returns A stream of the items to be rendered.
  //  */
  // connect(): Observable<SystemTableItem[]> {
  //   // Combine everything that affects the rendered data into one update
  //   // stream for the data-table to consume.
  //   const dataMutations = [
  //     observableOf(this.data),
  //     this.paginator.page,
  //     this.sort.sortChange
  //   ];

  //   return merge(...dataMutations).pipe(map(() => {
  //     return this.getPagedData(this.getSortedData([...this.data]));
  //   }));
  // }

  // /**
  //  *  Called when the table is being destroyed. Use this function, to clean up
  //  * any open connections or free any held resources that were set up during connect.
  //  */
  // disconnect() {}

  // /**
  //  * Paginate the data (client-side). If you're using server-side pagination,
  //  * this would be replaced by requesting the appropriate data from the server.
  //  */
  // private getPagedData(data: SystemTableItem[]) {
  //   const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
  //   return data.splice(startIndex, this.paginator.pageSize);
  // }

  // /**
  //  * Sort the data (client-side). If you're using server-side sorting,
  //  * this would be replaced by requesting the appropriate data from the server.
  //  */
  // private getSortedData(data: SystemTableItem[]) {
  //   if (!this.sort.active || this.sort.direction === '') {
  //     return data;
  //   }

  //   return data.sort((a, b) => {
  //     const isAsc = this.sort.direction === 'asc';
  //     switch (this.sort.active) {
  //       case 'location': return compare(a.location.id, b.location.id, isAsc);
  //       case 'connection': return compare(+a.connection, +b.connection, isAsc);
  //       case 'data': return compare(+a.data, +b.data, isAsc);
  //       default: return 0;
  //     }
  //   });
  // }

  getSystemPromise = (location, point, slave, date): Promise<any> => {
    return new Promise<any>(
      (resolve) => {
        try {
          let c = date;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${point}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "lsnr")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        |> count(column: "_value")
        `;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [];
              let cnt = 0;
              data.forEach(element => {
                console.log(element);
                cnt+=element._value;
              });
              d.push({
                location: location,
                connection: (cnt > 0) ? true: false,
                data : cnt
              })
              resolve(d);
            });
        } catch (error) {
          console.log(error);
        }
      }
    )
  }

  allSystemPromise(date) : any {
    const a = Promise.all<SystemTableItem>(
      [
        this.getSystemPromise(LOCATIONS[0], "P1", 1, date),  // จุดที่ 1
        this.getSystemPromise(LOCATIONS[1], "P2", 1, date),  // จุดที่ 2
        this.getSystemPromise(LOCATIONS[2], "P3", 1, date),  // จุดที่ 3
        this.getSystemPromise(LOCATIONS[3], "P5", 1, date),  // จุดที่ 4
        this.getSystemPromise(LOCATIONS[4], "P5", 2, date),  // จุดที่ 5
        this.getSystemPromise(LOCATIONS[5], "P4", 1, date),  // จุดที่ 6
        this.getSystemPromise(LOCATIONS[6], "P4", 2, date),  // จุดที่ 7
        this.getSystemPromise(LOCATIONS[7], "P6", 1, date),  // จุดที่ 8
        this.getSystemPromise(LOCATIONS[8], "P7", 1, date),  // จุดที่ 9
        this.getSystemPromise(LOCATIONS[9], "P7", 2, date),  // จุดที่ 10
        this.getSystemPromise(LOCATIONS[10], "P7", 3, date),  // จุดที่ 11
        this.getSystemPromise(LOCATIONS[11], "P8", 1, date),  // จุดที่ 12
        this.getSystemPromise(LOCATIONS[12], "P8", 2, date),  // จุดที่ 13
        this.getSystemPromise(LOCATIONS[13], "P8", 3, date),  // จุดที่ 14
      ]
    );
    return a;
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
// function compare(a: string | number, b: string | number, isAsc: boolean) {
//   return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
// }
