import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import {Local, LOCATIONS } from '../../services/location';
import { LocationService } from '../../services/location.service';

import {
  ApexNonAxisChartSeries,
  ApexAxisChartSeries,
  ApexTitleSubtitle,
  ApexResponsive,
  ApexChart,
  ApexFill,
  ApexDataLabels,
  ApexPlotOptions,
  ApexLegend,
  ApexXAxis,
  ApexYAxis,
  ApexStroke,
  ApexGrid,
  ApexTheme,
} from "ng-apexcharts";
import * as moment from 'moment';
import { QueryService } from 'src/app/services/query.service';
import { InfluxdbService } from 'src/app/services/influxdb.service';

export type ChartOptions = {
  series: ApexNonAxisChartSeries | ApexAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  fill: ApexFill;
  legend: ApexLegend;
  dataLabels: ApexDataLabels;
  colors: string[];
  plotOptions: ApexPlotOptions;
  title: ApexTitleSubtitle;
  x_axis: ApexXAxis;
  y_axis: ApexYAxis;
  grid: ApexGrid;
  stroke: ApexStroke;
  theme: ApexTheme;
};

@Component({
  selector: 'app-consume',
  templateUrl: './consume.component.html',
  styleUrls: ['./consume.component.scss']
})
export class ConsumeComponent implements OnInit {

  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  public chartsOptions: any[];

  selectedDate: any;

  dateChanged = (date) => {
    this.selectedDate = date;
    this.fetchData(date);
  }

  private influx: InfluxdbService;
  private query: QueryService;

  constructor(private localService: LocationService) {
    this.localService.date.subscribe(this.dateChanged);
    this.query = new QueryService();
    this.influx = new InfluxdbService();
    this.chartsOptions = [];
    for (let i = 0; i < LOCATIONS.length; i++)
    {
      this.chartsOptions.push({
        series: [
          {
            name: "R",
            data: [10, 41, 35, 51, 49, 62, 69, 91, 148, 54, 23, 56,
              10, 41, 35, 51, 49, 62, 69, 91, 148, 54, 23, 56]
          },
          {
            name: "S",
            data: [71, 1, 5, 15, 94, 26, 99, 19, 48, 87, 87, 12,
              71, 1, 5, 15, 94, 26, 99, 19, 48, 87, 87, 12,]
          },
          {
            name: "T",
            data: [100, 14, 53, 5, 9, 92, 9, 61, 84, 65, 23, 45,
              100, 14, 53, 5, 9, 92, 9, 61, 84, 65, 23, 45,]
          }
        ],
        chart: {
          height: 350,
          type: "line",
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight"
        },
        title: {
          text: LOCATIONS[i].name + ' Consumption',
          align: "left"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
            opacity: 0.5
          }
        },
        // x_axis: {
        //   categories: [
        //     "Jan",
        //     "Feb",
        //     "Mar",
        //     "Apr",
        //     "May",
        //     "Jun",
        //     "Jul",
        //     "Aug",
        //     "Sep"
        //   ]
        // }
      });
    }
  }

  getConsumptionPromise = (location, slave, date): Promise<any> => {
    return new Promise<any>(
      (resolve) => {
        try {
          let c = date;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "pA" or r["_field"] == "pB" or r["_field"] == "pC")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        `;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [
                {
                  name: "R",
                  data: []
                },
                {
                  name: "S",
                  data: []
                },
                {
                  name: "T",
                  data: []
                }];

                if (data.length > 0) {
                  data.forEach(element => {
                    if(element._field === 'pA')
                      d[0].data.push(element._value);
                    else if (element._field === 'pB')
                      d[1].data.push(element._value);
                    else if (element._field === 'pC')
                      d[2].data.push(element._value);
                  });
                } else {
                  // let n = [null, null,null, null,null, null,null, null,null, null,null, null,
                  //   null, null,null, null,null, null,null, null,null, null,null, null,];
                  let n = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,];

                    d[0].data = n;
                    d[1].data = n;
                    d[2].data = n;
                }


              // if(data.length === 0)
              // {
              //   for (let index = 0; index < 3; index++) {
              //     d.push(0);
              //   }
              // }
              resolve(d);
            });
        } catch (error) {
          console.log(error);
        }
      }
    )
  }

  allConsumptionPromise(date) : any {
    const a = Promise.all<any>(
      [
        this.getConsumptionPromise("P1", 1, date),  // จุดที่ 1
        this.getConsumptionPromise("P2", 1, date),  // จุดที่ 2
        this.getConsumptionPromise("P3", 1, date),  // จุดที่ 3
        this.getConsumptionPromise("P5", 1, date),  // จุดที่ 4
        this.getConsumptionPromise("P5", 2, date),  // จุดที่ 5
        this.getConsumptionPromise("P4", 1, date),  // จุดที่ 6
        this.getConsumptionPromise("P4", 2, date),  // จุดที่ 7
        this.getConsumptionPromise("P6", 1, date),  // จุดที่ 8
        this.getConsumptionPromise("P7", 1, date),  // จุดที่ 9
        this.getConsumptionPromise("P7", 2, date),  // จุดที่ 10
        this.getConsumptionPromise("P7", 3, date),  // จุดที่ 11
        this.getConsumptionPromise("P8", 1, date),  // จุดที่ 12
        this.getConsumptionPromise("P8", 2, date),  // จุดที่ 13
        this.getConsumptionPromise("P8", 3, date),  // จุดที่ 14
      ]
    );
    return a;
  }

  ngOnInit(): void {
    this.fetchData(this.selectedDate);
  }

  fetchData(date) : void {
    this.allConsumptionPromise(date).then((result) =>
    {
      console.log(result);
      for (let i = 0; i < LOCATIONS.length; i++)
      {
        this.chartsOptions[i].series = result[i];
      }
    });
  }

  onDateChange(event) {
    console.log(event);
    this.selectedDate= event;
    this.fetchData(event);
  }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
