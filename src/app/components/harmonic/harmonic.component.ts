import { Component, ViewChild, OnInit } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import {Local, LOCATIONS } from '../../services/location';
import { LocationService } from '../../services/location.service';

import {
  ApexNonAxisChartSeries,
  ApexAxisChartSeries,
  ApexTitleSubtitle,
  ApexResponsive,
  ApexChart,
  ApexFill,
  ApexDataLabels,
  ApexPlotOptions,
  ApexLegend,
  ApexXAxis,
  ApexYAxis,
  ApexStroke,
  ApexGrid,
  ApexTheme,
} from "ng-apexcharts";
import * as moment from 'moment';
import { InfluxdbService } from 'src/app/services/influxdb.service';
import { QueryService } from 'src/app/services/query.service';

export type ChartOptions = {
  series: ApexNonAxisChartSeries | ApexAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  fill: ApexFill;
  legend: ApexLegend;
  dataLabels: ApexDataLabels;
  colors: string[];
  plotOptions: ApexPlotOptions;
  title: ApexTitleSubtitle;
  x_axis: ApexXAxis;
  y_axis: ApexYAxis;
  grid: ApexGrid;
  stroke: ApexStroke;
  theme: ApexTheme;
};

@Component({
  selector: 'app-harmonic',
  templateUrl: './harmonic.component.html',
  styleUrls: [
    '../../../../node_modules/primeng/resources/themes/saga-blue/theme.css',
    '../../../../node_modules/primeng/resources/primeng.min.css',
    '../../../../node_modules/primeicons/primeicons.css',
    './harmonic.component.scss']
})
export class HarmonicComponent implements OnInit {

  public chartsOptions: any[];
  private influx: InfluxdbService;
  private query: QueryService;

  dateChanged = (date) => {
    this.selectedDate = date;
    this.fetchData(date);
  }
  constructor(private localService: LocationService) {
    this.localService.date.subscribe(this.dateChanged);
    this.query = new QueryService();
    this.influx = new InfluxdbService();
    this.chartsOptions = [];
    for (let i = 0; i < LOCATIONS.length; i++)
    {
      this.chartsOptions.push(
        {
          local: LOCATIONS[i],
            series: [
              {
                data: [50, 1]
              }
            ],
            chart: {
              type: "bar",
              height: 280
            },
            plotOptions: {
              bar: {
                barHeight: "40%",
                distributed: true,
                horizontal: true,
                dataLabels: {
                  position: "right"
                }
              }
            },
            colors: [
              "#33b2df",
              "#546E7A",
              "#d4526e",
              "#13d8aa",
              "#A5978B",
              "#2b908f",
              "#f9a3a4",
              "#90ee7e",
              "#f48024",
              "#69d2e7"
            ],
            dataLabels: {
              enabled: true,
              textAnchor: "start",
              style: {
                colors: ["#fff"]
              },
              formatter: function(val, opt) {
                return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val;
              },
              offsetX: 0,
              dropShadow: {
                enabled: true
              }
            },
            stroke: {
              width: 1,
              colors: ["#fff"]
            },
            x_axis: {
              categories: [
                "Hz",
                "Peak"
              ]
            },
            y_axis: {
              labels: {
                show: false
              }
            },
            title: {
              text: "สัญญาณรบกวนในระบบ",
              align: "center",
              floating: true
            },
            // subtitle: {
            //   text: "Category Names as DataLabels inside bars",
            //   align: "center"
            // },
            tooltip: {
              theme: "dark",
              x: {
                show: false
              },
              y: {
                title: {
                  formatter: function() {
                    return "";
                  }
                }
              }
            }


        }
      );
    }
  }

  getHarmonicPromise = (location, slave, date): Promise<any> => {
    return new Promise<any>(
      (resolve) => {
        try {
          let c = date;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "pD")
        |> aggregateWindow(every: duration(v: "1d"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [50,1];

                if (data.length > 0) {
                  data.forEach(element => {
                    if(element._value === 0)
                      d[1] = (1);
                    else
                      d[1] = (element._value);
                  });
                } else {
                  // let n = [null, null,null, null,null, null,null, null,null, null,null, null,
                  //   null, null,null, null,null, null,null, null,null, null,null, null,];
                  // d = [null,null,null];
                  // d = [0,0,0];
                  d = [50,1];
                }


              // if(data.length === 0)
              // {
              //   for (let index = 0; index < 3; index++) {
              //     d.push(0);
              //   }
              // }
              resolve(d);
            });
        } catch (error) {
          console.log(error);
        }
      }
    )
  }

  allHarmonicPromise(date) : any {
    const a = Promise.all<any>(
      [
        this.getHarmonicPromise("P1", 1, date),  // จุดที่ 1
        this.getHarmonicPromise("P2", 1, date),  // จุดที่ 2
        this.getHarmonicPromise("P3", 1, date),  // จุดที่ 3
        this.getHarmonicPromise("P5", 1, date),  // จุดที่ 4
        this.getHarmonicPromise("P5", 2, date),  // จุดที่ 5
        this.getHarmonicPromise("P4", 1, date),  // จุดที่ 6
        this.getHarmonicPromise("P4", 2, date),  // จุดที่ 7
        this.getHarmonicPromise("P6", 1, date),  // จุดที่ 8
        this.getHarmonicPromise("P7", 1, date),  // จุดที่ 9
        this.getHarmonicPromise("P7", 2, date),  // จุดที่ 10
        this.getHarmonicPromise("P7", 3, date),  // จุดที่ 11
        this.getHarmonicPromise("P8", 1, date),  // จุดที่ 12
        this.getHarmonicPromise("P8", 2, date),  // จุดที่ 13
        this.getHarmonicPromise("P8", 3, date),  // จุดที่ 14
      ]
    );
    return a;
  }

  ngOnInit(): void {
    this.fetchData(this.selectedDate);
  }

  fetchData(date) : void {
    this.allHarmonicPromise(date).then((result) =>
    {
      console.log(result);
      for (let i = 0; i < LOCATIONS.length; i++)
      {
        this.chartsOptions[i].series = [{ data:result[i] }];
      }
    });
  }

  selectedDate: Date = new Date();
  onDateChange(event) {
    console.log(event);
    this.selectedDate= event;
    this.fetchData(event);
  }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
