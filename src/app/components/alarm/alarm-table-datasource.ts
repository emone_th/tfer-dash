import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { catchError, map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import { InfluxdbService } from 'src/app/services/influxdb.service';
import { QueryService } from 'src/app/services/query.service';
import * as moment from 'moment';
import { LOCATIONS } from 'src/app/services/location';

// TODO: Replace this with your own data model type
export interface AlarmTableItem {
  location: string;
  slave : string;
  mode : string;
  detail: string;
  date: string;
  status: boolean;
}

// TODO: replace this with real data from your application
// const EXAMPLE_DATA: AlarmTableItem[] = [
//   { date: '2020-10-01T08:00', status: true, detail: 'None'},
//   { date: '2020-10-02T08:00', status: true, detail: 'None'},
//   { date: '2020-10-03T08:00', status: true, detail: 'None'},
//   { date: '2020-10-04T08:00', status: false, detail: 'None'},
//   { date: '2020-10-05T08:00', status: true, detail: 'None'},
//   { date: '2020-10-06T08:00', status: true, detail: 'None'},
//   { date: '2020-10-07T08:00', status: true, detail: 'None'},
//   { date: '2020-10-08T08:00', status: false, detail: 'None'},
//   { date: '2020-10-09T08:00', status: true, detail: 'None'},
//   { date: '2020-10-10T08:00', status: true, detail: 'None'},
//   { date: '2020-10-11T08:00', status: true, detail: 'None'},
//   { date: '2020-10-12T08:00', status: true, detail: 'None'},
//   {date: '2020-10-13T08:00', status: true, detail: 'None'},
//   { date: '2020-10-14T08:00', status: true, detail: 'None'},
//   { date: '2020-10-15T08:00', status: true, detail: 'None'},
//   { date: '2020-10-16T08:00', status: true, detail: 'None'},
//   { date: '2020-10-17T08:00', status: true, detail: 'None'},
//   { date: '2020-10-18T08:00', status: true, detail: 'None'},
//   { date: '2020-10-19T08:00', status: true, detail: 'None'},
//   { date: '2020-10-20T08:00', status: true, detail: 'None'},
// ];

/**
 * Data source for the AlarmTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class AlarmTableDataSource //extends DataSource<AlarmTableItem>
{
  data: AlarmTableItem[] = [];//EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;
  private influx: InfluxdbService;
  private query: QueryService;

  constructor(private date: Date) {
    // super();
    this.query = new QueryService();
    this.influx = new InfluxdbService();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  // connect(): Observable<AlarmTableItem[]> {
  //   // Combine everything that affects the rendered data into one update
  //   // stream for the data-table to consume.
  //   const dataMutations = [
  //     observableOf(this.allAlarmPromise(this.date).then(
  //       (result) => {
  //         let d: AlarmTableItem[] = []
  //         console.log(result);
  //         this.data = d;
  //       }
  //     )),
  //     this.paginator.page,
  //     this.sort.sortChange
  //   ];

  //   return merge(...dataMutations).pipe(
  //     map(() => {
  //       return this.data;//this.getPagedData(this.getSortedData([...this.data]));
  //     })
  //   );
  // }

  // /**
  //  *  Called when the table is being destroyed. Use this function, to clean up
  //  * any open connections or free any held resources that were set up during connect.
  //  */
  // disconnect() {}

  // /**
  //  * Paginate the data (client-side). If you're using server-side pagination,
  //  * this would be replaced by requesting the appropriate data from the server.
  //  */
  // private getPagedData(data: AlarmTableItem[]) {
  //   const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
  //   return data.splice(startIndex, this.paginator.pageSize);
  // }

  // /**
  //  * Sort the data (client-side). If you're using server-side sorting,
  //  * this would be replaced by requesting the appropriate data from the server.
  //  */
  // private getSortedData(data: AlarmTableItem[]) {
  //   if (!this.sort.active || this.sort.direction === '') {
  //     return data;
  //   }

  //   return data.sort((a, b) => {
  //     const isAsc = this.sort.direction === 'asc';
  //     switch (this.sort.active) {
  //       case 'detail': return compare(a.detail, b.detail, isAsc);
  //       case 'date': return compare(+a.date, +b.date, isAsc);
  //       default: return 0;
  //     }
  //   });
  // }

  getAlarmPromise = (location, point, slave, date): Promise<any> => {
    return new Promise<any>(
      (resolve) => {
        try {
          let c = date;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${point}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "rssi")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        `;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [];
              data.forEach(element => {
                console.log(element);
                d.push(
                  {
                    location: location.name,
                    mode: element.mode,
                    detail: element._value,
                    date: element._time,
                    status: element._value === null ? false : true,
                  });
              });
              resolve(d);
            });
        } catch (error) {
          console.log(error);
        }
      }
    )
  }

  allAlarmPromise(date) : any {
    const a = Promise.all<AlarmTableItem>(
      [
        this.getAlarmPromise(LOCATIONS[0], "P1", 1, date),  // จุดที่ 1
        this.getAlarmPromise(LOCATIONS[1], "P2", 1, date),  // จุดที่ 2
        this.getAlarmPromise(LOCATIONS[2], "P3", 1, date),  // จุดที่ 3
        this.getAlarmPromise(LOCATIONS[3], "P5", 1, date),  // จุดที่ 4
        this.getAlarmPromise(LOCATIONS[4], "P5", 2, date),  // จุดที่ 5
        this.getAlarmPromise(LOCATIONS[5], "P4", 1, date),  // จุดที่ 6
        this.getAlarmPromise(LOCATIONS[6], "P4", 2, date),  // จุดที่ 7
        this.getAlarmPromise(LOCATIONS[7], "P6", 1, date),  // จุดที่ 8
        this.getAlarmPromise(LOCATIONS[8], "P7", 1, date),  // จุดที่ 9
        this.getAlarmPromise(LOCATIONS[9], "P7", 2, date),  // จุดที่ 10
        this.getAlarmPromise(LOCATIONS[10], "P7", 3, date),  // จุดที่ 11
        this.getAlarmPromise(LOCATIONS[11], "P8", 1, date),  // จุดที่ 12
        this.getAlarmPromise(LOCATIONS[12], "P8", 2, date),  // จุดที่ 13
        this.getAlarmPromise(LOCATIONS[13], "P8", 3, date),  // จุดที่ 14
      ]
    );
    return a;
  }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
