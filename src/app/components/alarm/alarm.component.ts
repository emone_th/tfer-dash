import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AlarmTableDataSource, AlarmTableItem } from './alarm-table-datasource';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import { LocationService } from 'src/app/services/location.service';
import { element } from 'protractor';

@Component({
  selector: 'app-alarm',
  templateUrl: './alarm.component.html',
  styleUrls: ['./alarm.component.scss']
})
export class AlarmComponent implements AfterViewInit, OnInit  {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<AlarmTableItem>;
  dataSource: AlarmTableDataSource | null;
  data: AlarmTableItem[] = [];
  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['location', 'mode', 'date', 'status', 'detail'];

  isLoadingResults = true;
  isRateLimitReached = false;

  selectedDate: Date = new Date();
  dateChanged = (date) => {
    this.selectedDate = date;
    this.data = [];
    this.ngAfterViewInit();
  }

  constructor(private localService: LocationService) {
    this.localService.date.subscribe(this.dateChanged);

  }

  ngOnInit(): void {
  }

  onDateChange(event) {
    console.log(event);
    this.selectedDate= event;
    this.data = [];
    this.ngAfterViewInit();
  }

  ngAfterViewInit() {
    let d: AlarmTableItem[] = [];
    this.dataSource = new AlarmTableDataSource(this.selectedDate);
    // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.isLoadingResults = true;
    this.dataSource.allAlarmPromise(this.selectedDate).then((result) => {

      result.forEach(element => {
        if(element.length > 0)
        {

          console.log(element);
          element.forEach(e => {
            d.push(e);
          })

        }
      })
    }).then(()=>{
      this.data = d;
    }).finally(() => {
      this.isLoadingResults = false;
      this.isRateLimitReached = false;
    });
  }
  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: AlarmTableItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: AlarmTableItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'detail': return compare(a.detail, b.detail, isAsc);
        case 'date': return compare(+a.date, +b.date, isAsc);
        default: return 0;
      }
    });
  }
}
// /** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
