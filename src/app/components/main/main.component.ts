import { Component, ViewChild, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { InfluxdbService } from '../../services/influxdb.service'
import { QueryService } from '../../services/query.service';
import {MatCalendar,MatDatepickerInputEvent} from '@angular/material/datepicker';

import {
  ChartComponent,
  ApexNonAxisChartSeries,
  ApexAxisChartSeries,
  ApexTitleSubtitle,
  ApexResponsive,
  ApexChart,
  ApexFill,
  ApexDataLabels,
  ApexPlotOptions,
  ApexLegend,
  ApexXAxis,
  ApexYAxis,
  ApexStroke,
  ApexGrid,
  ApexTheme,
} from "ng-apexcharts";

import { LOCATIONS } from 'src/app/services/location';
// import { Route } from '@angular/compiler/src/core';
import {Router, ActivatedRoute, NavigationStart, NavigationEnd} from '@angular/router';
import { LocationService } from 'src/app/services/location.service';
import * as moment from 'moment';
import { rejects } from 'assert';

export type ChartOptions = {
  series: ApexNonAxisChartSeries | ApexAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  fill: ApexFill;
  legend: ApexLegend;
  dataLabels: ApexDataLabels;
  colors: string[];
  plotOptions: ApexPlotOptions;
  title: ApexTitleSubtitle;
  x_axis: ApexXAxis;
  y_axis: ApexYAxis;
  grid: ApexGrid;
  stroke: ApexStroke;
  theme: ApexTheme;
};

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  totalEnergy: string;
  totalExpense = function(energys): string {
    return (parseFloat(this.totalEnergy) * 4.2).toFixed(2);
  } ;

  @ViewChild("piechart") piechart: ChartComponent;
  public piechartOptions: Partial<ChartOptions>;

  @ViewChild("linechart") linechart: ChartComponent;
  public linechartOptions: Partial<ChartOptions>;

  @ViewChild("barchart") barchart: ChartComponent;
  public barchartOptions: Partial<ChartOptions>;

  @ViewChild("mapchart") mapchart: ChartComponent;
  public mapchartOptions: Partial<ChartOptions>;

  options: any;

  overlays: any[];

  // @Input() datetime:Date;

  private influx: InfluxdbService;
  private query: QueryService;
  // google: GMap;

  // isPromise = async(location: string, slave:number) =>  {
  //   try
  //   {
  //     let c = this.selectedDate;
  //     c.setHours(23,59,59);
  //     let e = c.toISOString();
  //     let s = moment(c).add(-1, 'day').toISOString();
  //     const q = await this.query.ParamStringMean("tfer", "tfer_pm",
  //                     "1d", s, e,
  //                     location, slave.toString(),
  //                     ["pA"],
  //                     "mean", "false");
  //     return q;
  //   }
  //   catch(e)
  //   {
  //     console.log(e);
  //     return 10;
  //   } finally {

  //   }
  // };

  getPromise = (location: string, slave: number): Promise<number> => {
    return new Promise<number>((resolve,reject) => {
      try{
        const c = this.selectedDate;
        c.setHours(23,59,59);
        let e = c.toISOString();
        let s = moment(c).add(-1, 'day').toISOString();
        const q =  this.query.ParamStringMean("tfer", "tfer_pm",
                      "1d", s, e,
                      location, slave.toString(),
                      ["pT"],
                      "mean", "false");
        // this.isPromise(location, slave).then(q => {
          this.influx.queryFluxString(q).then(data =>
            {
              if(data.length > 0)
                resolve(data[0]._value);
              else
                resolve(1);
            });
        // });
      }
      catch(e)
      {
        console.log(e);
        reject(10);
      }
    });
  };

  allPromise(date) : any {
    const a = Promise.all<number>( [
      this.getPromise("P1", 1),  // จุดที่ 1
      this.getPromise("P2", 1),  // จุดที่ 2
      this.getPromise("P3", 1),  // จุดที่ 3
      this.getPromise("P5", 1),  // จุดที่ 4
      this.getPromise("P5", 2),  // จุดที่ 5
      this.getPromise("P4", 1),  // จุดที่ 6
      this.getPromise("P4", 2),  // จุดที่ 7
      this.getPromise("P6", 1),  // จุดที่ 8
      this.getPromise("P7", 1),  // จุดที่ 9
      this.getPromise("P7", 2),  // จุดที่ 10
      this.getPromise("P7", 3),  // จุดที่ 11
      this.getPromise("P8", 1),  // จุดที่ 12
      this.getPromise("P8", 2),  // จุดที่ 13
      this.getPromise("P8", 3),  // จุดที่ 14
    ] );
    return a;
  };

  alldayofMonth(d: Date) : any {
    const a = Promise.all<any>( [
      this.getDatesPromise("P1", 1, d),  // จุดที่ 1
      this.getDatesPromise("P2", 1, d),  // จุดที่ 2
      this.getDatesPromise("P3", 1, d),  // จุดที่ 3
      this.getDatesPromise("P5", 1, d),  // จุดที่ 4
      this.getDatesPromise("P5", 2, d),  // จุดที่ 5
      this.getDatesPromise("P4", 1, d),  // จุดที่ 6
      this.getDatesPromise("P4", 2, d),  // จุดที่ 7
      this.getDatesPromise("P6", 1, d),  // จุดที่ 8
      this.getDatesPromise("P7", 1, d),  // จุดที่ 9
      this.getDatesPromise("P7", 2, d),  // จุดที่ 10
      this.getDatesPromise("P7", 3, d),  // จุดที่ 11
      this.getDatesPromise("P8", 1, d),  // จุดที่ 12
      this.getDatesPromise("P8", 2, d),  // จุดที่ 13
      this.getDatesPromise("P8", 3, d),  // จุดที่ 14
    ] );
    return a;
  };

  allMonthofYear(d: Date) : any {
    const a = Promise.all<any>( [
      this.getMonthsPromise("P1", 1, d),  // จุดที่ 1
      this.getMonthsPromise("P2", 1, d),  // จุดที่ 2
      this.getMonthsPromise("P3", 1, d),  // จุดที่ 3
      this.getMonthsPromise("P5", 1, d),  // จุดที่ 4
      this.getMonthsPromise("P5", 2, d),  // จุดที่ 5
      this.getMonthsPromise("P4", 1, d),  // จุดที่ 6
      this.getMonthsPromise("P4", 2, d),  // จุดที่ 7
      this.getMonthsPromise("P6", 1, d),  // จุดที่ 8
      this.getMonthsPromise("P7", 1, d),  // จุดที่ 9
      this.getMonthsPromise("P7", 2, d),  // จุดที่ 10
      this.getMonthsPromise("P7", 3, d),  // จุดที่ 11
      this.getMonthsPromise("P8", 1, d),  // จุดที่ 12
      this.getMonthsPromise("P8", 2, d),  // จุดที่ 13
      this.getMonthsPromise("P8", 3, d),  // จุดที่ 14
    ] );
    return a;
  };

  getDatesPromise = (location: string, slave:number, date: Date) => {
    return new Promise<any>((resolve,reject) => {
      try{
        let d = [];
        const now = date;
        now.setHours(23,59,59);
        const c = new Date(now.getFullYear(), now.getMonth()+1, 0);
        const days = c.getDate();
        c.setHours(23,59,59);
        // for (let index = 1; index <= days; index++) {
          let e = c.toISOString();
          let s = moment(e).add(days* -1, 'day').toISOString();
          let q = this.query.ParamString("tfer", "tfer_pm", "1d", s, e, location, slave.toString(),
                          ["pT"], "mean", "true");

          this.influx.queryFluxString(q).then(data => {
            if(data.length > 0) {
              data.forEach(element => {
                d.push((element._value === null)? 0: element._value);
              });
              d.pop();
            } else {
              for (let index = 0; index < days; index++) {
                d.push(0);
              }
            }
            resolve(d);
          });
        // }

      }
      catch(e)
      {
        console.log(e);
        reject(10);
      }
    });

  };

  getMonthsPromise = (location: string, slave:number, date: Date) => {
    return new Promise<any>((resolve,reject) => {
      try{
        let d = [];
        let now = new Date();
        let c = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        const days = c.getDate();
        c.setMonth(0,1);
        c.setHours(23,59,59);
        let e = moment(c).add(1, 'year').toISOString();
        let s = moment(c).add(0, 'day').toISOString();
        let q = this.query.ParamString("tfer", "tfer_pm", "30d", s, e, location, slave.toString(),
                        ["pT"], "mean", "true");

        this.influx.queryFluxString(q).then(data => {
          if(data.length > 0) {
            data.forEach(element => {
              d.push((element._value === null)? 0: element._value);
            });
            d.pop();
          } else {
            for (let index = 0; index < days; index++) {
              d.push(0);
            }
          }
          resolve(d);
        });
      }
      catch(e)
      {
        let d = [];
        console.log(e);
        for (let index = 0; index < 12; index++) {
          d.push(Math.random());
        }
        reject(d);
      }
    });

  };

  dateChanged = (date: Date) => {
    this.selectedDate = date;
    this.newMethod(date);
  };

  constructor(private route: Router,
    private activateRoute:ActivatedRoute,
    private localService: LocationService
    )
  {
    this.totalEnergy = "10000";

    this.localService.date.subscribe(this.dateChanged);
    this.piechartOptions = {
      title: {
       text: "Power consumtion",
       align: "center"
      },
      series: [44, 55, 41, 17, 15, 90, 70, 11, 45, 34, 23, 99, 80, 70],
      chart: {
        width: 450,
        height: 450,
        type: "donut"
      },
      labels: [
        "P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9", "P10", "P11", "P12", "P13", "P14",
      ],
      colors: [
        "#33b2df",
        "#546E7A",
        "#d4526e",
        "#13d8aa",
        "#A5978B",
        "#2b908f",
        "#f9a3a4",
        "#90ee7e",
        "#f48024",
        "#69d2e7",
        "#666633",
        "#f00c0c",
        "#6600cc",
        "#f0ec0c",
      ],
      fill: {
        type: "gradient",
        colors: [
          "#33b2df",
          "#546E7A",
          "#d4526e",
          "#13d8aa",
          "#A5978B",
          "#2b908f",
          "#f9a3a4",
          "#90ee7e",
          "#f48024",
          "#69d2e7",
          "#666633",
          "#f00c0c",
          "#6600cc",
          "#f0ec0c",
        ],
      },
      // legend: {
      //   formatter: function(val, opts) {
      //     return val + " - " + opts.w.globals.series[opts.seriesIndex];
      //   },
      // },
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };

    this.mapchartOptions = {
      series: [
        {
          data: [
            {
              x: "P1",
              y: 200
            },
            {
              x: "P2",
              y: 184
            },
            {
              x: "P3",
              y: 84
            },
            {
              x: "P4",
              y: 155
            },
            {
              x: "P5",
              y: 31
            },
            {
              x: "P6",
              y: 31
            },
            {
              x: "P7",
              y: 70
            },
            {
              x: "P8",
              y: 130
            },
            {
              x: "P9",
              y: 130
            },
            {
              x: "P10",
              y: 130
            },
            {
              x: "P11",
              y: 130
            },
            {
              x: "P12",
              y: 130
            },
            {
              x: "P13",
              y: 130
            },
            {
              x: "P14",
              y: 130
            },
          ]
        }
      ],
      legend: {
        show: false
      },
      chart: {
        height: 300,
        type: "treemap"
      },
      title: {
        text: "Distibuted Treemap (TFER plant)",
        align: "center"
      },
      plotOptions: {
        treemap: {
          distributed: true,
          enableShades: true
        }
      },
      colors: [
        "#33b2df",
        "#546E7A",
        "#d4526e",
        "#13d8aa",
        "#A5978B",
        "#2b908f",
        "#f9a3a4",
        "#90ee7e",
        "#f48024",
        "#69d2e7",
        "#666633",
        "#f00c0c",
        "#6600cc",
        "#f0ec0c",
      ],
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };

    this.barchartOptions = {
      series: [
        {
          name: "Consumtion",
          data: [10, 41, 35, 51, 49, 62, 69, 91, 148, 50, 98, 110]
        }
      ],
      chart: {
        type: "bar",
        height: 380,
        zoom: {
          enabled: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: false

        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "straight"
      },
      title: {
        text: "(kWh)",
        align: "left"
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      // x_axis: {
      //   categories: [
      //     "Jan",
      //     "Feb",
      //     "Mar",
      //     "Apr",
      //     "May",
      //     "Jun",
      //     "Jul",
      //     "Aug",
      //     "Sep",
      //     "Oct",
      //     "Nov",
      //     "Dec",
      //   ]
      // }
    };

    this.linechartOptions = {
      series: [
        {
          name: "Consumption",
          data: [10, 41, 35, 51, 49, 62, 69, 91, 148, 50, 98, 110]
        }
      ],
      chart: {
        height: 380,
        type: "line",
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "straight"
      },
      title: {
        text: "Consumption Statistics by Month",
        align: "left"
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      x_axis: {
        categories: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec",
        ]
      }
    };

    this.query = new QueryService();
    this.influx = new InfluxdbService();
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.


    // this.getOverall();
    // this.newMethod(this.selectedDate);

    this.options = {
      center: {lat: 14.409404, lng: 100.5787169},
      zoom: 12
    };
  }

  private todayComsumption(date:Date){
    console.time('settled-in');
    this.allPromise(date).then((result) => {
      console.log(result);
      this.piechartOptions.series = result;
      let d = this.mapchartOptions.series.pop();
      for (const x in result) {
        d["data"][x].y = result[x];
      }
      this.mapchartOptions.series =  [ { data: d["data"] } ];

        let t = 0;
        for (const x of result) {
          t += (x*24);
        }
        this.totalEnergy =  t.toFixed(2);

    }).catch((err) => {
      console.log(err);
    }).finally(
      () => {
        console.timeEnd('settled-in');
      }
    );
  }

  private dailyConsumption(date:Date)
  {
    console.time('dailyConsumption settled-in');
    this.alldayofMonth(date).then((result) => {
      console.log(result);
      for (let x = 1; x < result.length; x++) {
        for (let y = 0; y < result[x].length; y++) {
            result[0][y] += result[x][y];
        }
      }
      // if(result[0].length > 0) {
        let d = this.barchartOptions.series.pop();
        this.barchartOptions.series = [ { name : "consumption", data: result[0]}];
      // }
    }).catch((err) => {
      console.log(err);
    }).finally(
      () => {
        console.timeEnd('dailyConsumption settled-in');
      }
    );

  }

  private monthlyConsumption(date:Date)
  {
    // let m = []
    // let months = new Date(date.getFullYear(),0, 0).getMonth();
    // for (let index = 0; index <= months; index++) {
    //   if (date.getFullYear() == new Date().getFullYear()) {
    //     if(index <= date.getMonth())
    //       m.push(parseFloat(Math.random().toFixed(2)));
    //       //TODO : Get montly power consumption
    //     else
    //       m.push(0);
    //   } else {
    //     m.push(parseFloat(Math.random().toFixed(2)));
    //     //TODO : Get montly power consumption
    //   }
    // }
    // this.linechartOptions.series = [ { name : "consumption",data: m}];

    console.time('monthlyConsumption settled-in');
    this.allMonthofYear(date).then((result) => {
      console.log(result);
      for (let x = 1; x < result.length; x++) {
        for (let y = 0; y < result[x].length; y++) {
            result[0][y] += result[x][y];
        }
      }
      if(result[0].length > 0) {
        let d = this.linechartOptions.series.pop();
        this.linechartOptions.series = [ { name : "consumption", data: result[0]}];
      }
    }).catch((err) => {
      console.log(err);
    }).finally(
      () => {
        console.timeEnd('monthlyConsumption settled-in');
      }
    );

  }

  private newMethod(date) {
    this.todayComsumption(date);
    this.dailyConsumption(date);
    this.monthlyConsumption(date);
  }

  // private getOverall() {
  //   let x = this.query.getPowerOverAll;
  //   let cA = this.influx.queryFluxString(x);

  // }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    console.log(changes);
  }

  selectLocation(event: any): void {
    console.log("location selected");
  }

  handleMapClick(event: any) {
      //event: MouseEvent of Google Maps api
  }

  handleOverlayClick(event: any) {
      //event.originalEvent: MouseEvent of Google Maps api
      //event.overlay: Clicked overlay
      //event.map: Map instance
  }

  onSelect(event){
    console.log(event);
    this.selectedDate= event;
  }

  selectedDate: Date;
  onDateChange(event:Date) {
    console.log(event.toISOString());
    this.selectedDate= event;
    this.newMethod(event);
  }
}
