import { Component, ViewChild, OnInit, OnDestroy, SimpleChanges, OnChanges } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { Menu, Header, Content, NavService, MenuSectionItem  } from 'ngx-simple-dashboard';
import { LOCATIONS, Local, locationList, slaveList } from '../../services/location';
import { LocationService } from '../../services/location.service';

// import {} from 'primeng/blockui';

// import { InfluxdbService } from '../../services/influxdb.service'

// import {MatGridListModule} from '@angular/material/grid-list';

import {
  ChartComponent,
  ApexNonAxisChartSeries,
  ApexAxisChartSeries,
  ApexTitleSubtitle,
  ApexResponsive,
  ApexChart,
  ApexFill,
  ApexDataLabels,
  ApexPlotOptions,
  ApexLegend,
  ApexXAxis,
  ApexYAxis,
  ApexStroke,
  ApexGrid,
  ApexTheme,
} from "ng-apexcharts";
import * as moment from 'moment';
import { InfluxdbService } from 'src/app/services/influxdb.service';
import { QueryService } from 'src/app/services/query.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { zip } from 'rxjs';

export type ChartOptions = {
  series: ApexNonAxisChartSeries | ApexAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  fill: ApexFill;
  legend: ApexLegend;
  dataLabels: ApexDataLabels;
  colors: string[];
  plotOptions: ApexPlotOptions;
  title: ApexTitleSubtitle;
  x_axis: ApexXAxis;
  y_axis: ApexYAxis;
  grid: ApexGrid;
  stroke: ApexStroke;
  theme: ApexTheme;
};

@Component({
  templateUrl: './location.component.html',
  styleUrls: [
    './location.component.scss',
    '../../../../node_modules/primeng/resources/themes/saga-blue/theme.css',
    '../../../../node_modules/primeng/resources/primeng.min.css',
    '../../../../node_modules/primeicons/primeicons.css'
  ]

})
export class LocationComponent implements OnInit, OnChanges, OnDestroy {

  isOnline = true;
  isOffline = false;

  locations = LOCATIONS;
  public selectedLocation: Local;

  electricHourList = {
    kw:[],
    kvar:[],
    kva:[],
  }

  getHoursConsumptionPromise = (location: string, slave: number, date:Date): Promise<any> => {
    return new Promise<any>((resolve,reject) => {
      try{
          let c = date;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          // const q = this.query.ParamStringMean("tfer", "tfer_pm",
          //                 "60m", s, e,
          //                 location, slave.toString(),
          //                 ["cA"],
          //                 "mean", "true");
         const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "pT" or r["_field"] == "pfT")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)`;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [];
              let pF = [];
              let kw = [];
              let kva = [];
              let kvar = [];

              data.forEach(element => {
                if(element._field === 'pT')
                  kw.push(element._value);
                // else if(element._field === 'vLLA')
                //   v.push(element._value);
                // else if(element._field === 'cA')
                //   c.push(element._value);
                else if(element._field === 'pfT') {

                  pF.push(element._value);
                }
                // d.push(element._value);
              });
              if(data.length === 0)
              {
                for (let index = 0; index < 24; index++) {
                  pF.push(0); kw.push(0); kva.push(0); kvar.push(0);
                }
              }
              else
              {
                for (let index = 0; index < kw.length; index++) {
                  const p = kw[index];
                  //S=V*I*Cos(zeta) ; zeta = cos^-1 (S/(V*I))
                  if (p == 0 || p == null || pF[index] == null) {
                    pF.push(0);
                    kvar.push(null);
                    kva.push(null);
                  } else {
                    pF[index] = (pF[index] > 1) ? 2-pF[index] : pF[index];
                    kva.push(kw[index] / (pF[index]));
                    kvar.push(Math.sqrt(Math.pow(kva[index],2) - Math.pow(kw[index],2)));
                  }

                }
              }

              d = [{
                kw: kw,
                kvar: kvar,
                kva: kva
              }];
              resolve(d);
            });

      }
      catch(e)
      {
        console.log(e);
        let d = [];
        let p1 = [], p2= [], p3 = [];
        for (let index = 0; index < 24; index++) {
          p1.push(0); p2.push(0); p3.push(0);
        }
        d = [{
          kw: p1,
          kvar: p2,
          kva: p3
        }];
        reject(d);
      }
    });
  };

  getPowerPromise = (location: string, slave: number, date:Date): Promise<number[]> => {
    return new Promise<number[]>((resolve,reject) => {
      try{
          let c = date;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          // const q = this.query.ParamStringMean("tfer", "tfer_pm",
          //                 "60m", s, e,
          //                 location, slave.toString(),
          //                 ["cA"],
          //                 "mean", "true");
         const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "pA" or r["_field"] == "pB" or r["_field"] == "pC" or r["_field"] == "pT")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [];
              data.forEach(element => {
                d.push(element._value);
              });
              if(data.length === 0)
              {
                for (let index = 0; index < 4; index++) {
                  d.push(0);
                }
              }
              resolve(d);
            });

      }
      catch(e)
      {
        console.log(e);
        let d = [];
        for (let index = 0; index < 24; index++) {
          d.push(0);
        }
        reject(d);
      }
    });
  };

  getUnbalancePromise = (location: string, slave: number, date:Date): Promise<number[]> => {
    return new Promise<number[]>((resolve,reject) => {
      try{
          let c = date;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          // const q = this.query.ParamStringMean("tfer", "tfer_pm",
          //                 "60m", s, e,
          //                 location, slave.toString(),
          //                 ["cA"],
          //                 "mean", "true");
         const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "cUA" or r["_field"] == "cUB" or r["_field"] == "cUC")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [];
              data.forEach(element => {
                d.push(element._value);
              });
              if(data.length === 0)
              {
                for (let index = 0; index < 3; index++) {
                  d.push(0);
                }
              }
              resolve(d);
            });

      }
      catch(e)
      {
        console.log(e);
        let d = [];
        for (let index = 0; index < 24; index++) {
          d.push(0);
        }
        reject(d);
      }
    });
  };

  getVoltage3PPromise = (location: string, slave: number, date:Date): Promise<number[]> => {
    return new Promise<number[]>((resolve,reject) => {
      try{
        let c = date;
        c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          // const q = this.query.ParamStringMean("tfer", "tfer_pm",
          //                 "60m", s, e,
          //                 location, slave.toString(),
          //                 ["cA"],
          //                 "mean", "true");
         const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "vAB" or r["_field"] == "vBC" or r["_field"] == "vCA")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [];
              data.forEach(element => {
                d.push(element._value);
              });
              if(data.length === 0)
              {
                for (let index = 0; index < 3; index++) {
                  d.push(0);
                }
              }
              resolve(d);
            });

      }
      catch(e)
      {
        console.log(e);
        let d = [];
        for (let index = 0; index < 24; index++) {
          d.push(0);
        }
        reject(d);
      }
    });
  };

  getVoltage2PPromise = (location: string, slave: number, date:Date): Promise<number[]> => {
    return new Promise<number[]>((resolve,reject) => {
      try{
        let c = date;
        c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          // const q = this.query.ParamStringMean("tfer", "tfer_pm",
          //                 "60m", s, e,
          //                 location, slave.toString(),
          //                 ["cA"],
          //                 "mean", "true");
         const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "vAN" or r["_field"] == "vBN" or r["_field"] == "vCN")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data => {
              let d = [];
              data.forEach(element => {
                d.push(element._value);
              });
              if(data.length === 0)
              {
                for (let index = 0; index < 3; index++) {
                  d.push(0);
                }
              }
              resolve(d);
            });

      }
      catch(e)
      {
        console.log(e);
        let d = [];
        for (let index = 0; index < 24; index++) {
          d.push(0);
        }
        reject(d);
      }
    });
  };

  getAmperePromise = (location: string, slave: number, date:Date): Promise<number[]> => {
    return new Promise<number[]>((resolve,reject) => {
      try{
        let c = date;
        c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          // const q = this.query.ParamStringMean("tfer", "tfer_pm",
          //                 "60m", s, e,
          //                 location, slave.toString(),
          //                 ["cA"],
          //                 "mean", "true");
         const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "cA" or r["_field"] == "cB" or r["_field"] == "cC")
        |> aggregateWindow(every: duration(v: "1h"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [];
              data.forEach(element => {
                d.push(element._value);
              });
              if(data.length === 0)
              {
                for (let index = 0; index < 3; index++) {
                  d.push(0);
                }
              }
              resolve(d);
            });

      }
      catch(e)
      {
        console.log(e);
        let d = [];
        for (let index = 0; index < 24; index++) {
          d.push(0);
        }
        reject(d);
      }
    });
  };

  // locationList = [ "P1", "P2", "P3", "P5", "P5", "P4", "P4", "P6", "P7", "P7", "P7", "P8", "P8", "P8"];
  // slaveList = [1,1,1,1,2,1,2,1,1,2,3,1,2,3];

  allPromise(id, date) : any {
    const a = Promise.all<number[]>( [
      this.getHoursConsumptionPromise(locationList[id], slaveList[id], date),
      this.getPowerPromise(locationList[id], slaveList[id], date),
      this.getUnbalancePromise(locationList[id], slaveList[id], date),
      this.getVoltage3PPromise(locationList[id], slaveList[id], date),
      this.getVoltage2PPromise(locationList[id], slaveList[id], date),
      this.getAmperePromise(locationList[id], slaveList[id], date),
      // this.getPromise("P1", 1),  // จุดที่ 1
      // this.getPromise("P2", 1),  // จุดที่ 2
      // this.getPromise("P3", 1),  // จุดที่ 3
      // this.getPromise("P5", 1),  // จุดที่ 4
      // this.getPromise("P5", 2),  // จุดที่ 5
      // this.getPromise("P4", 1),  // จุดที่ 6
      // this.getPromise("P4", 2),  // จุดที่ 7
      // this.getPromise("P6", 1),  // จุดที่ 8
      // this.getPromise("P7", 1),  // จุดที่ 9
      // this.getPromise("P7", 2),  // จุดที่ 10
      // this.getPromise("P7", 3),  // จุดที่ 11
      // this.getPromise("P8", 1),  // จุดที่ 12
      // this.getPromise("P8", 2),  // จุดที่ 13
      // this.getPromise("P8", 3),  // จุดที่ 14
    ] );
    return a;
  };

  header: Header = {
    title: 'Area dashboard',
    color: 'primary',
  };

  menu: Menu = {
    sections: [
      {
        title: 'Navigation',
        items: [],
      }
    ]
  };

  content: Content = {
    header: {
      color: 'primary',
    }
  }

  electric =
    {
      P3: [380, 381, 382],
      P2: [220, 221, 222],
      A: [20, 21, 22],
    };

  @ViewChild("linechart") linechart: ChartComponent;
  public linechartOptions: Partial<ChartOptions>;
  @ViewChild("vbarchart") vbarchart: ChartComponent;
  public vbarchartOptions: Partial<ChartOptions>;
  @ViewChild("hbarchart") hbarchart: ChartComponent;
  public hbarchartOptions: Partial<ChartOptions>;
  @ViewChild("piechart") piechart: ChartComponent;
  public piechartOptions: Partial<ChartOptions>;

  dateChanged = (date) => {
    this.selectedDate = date;
    this.fetchData(date);
  }

  private influx: InfluxdbService;
  private query: QueryService;
  lineData = [141, 135, 151, 149, 162, 169, 91, 148, 150, 98, 110, 98,
    141, 135, 151, 149, 162, 169, 91, 148, 150, 98, 110 ,68];
  constructor(
    private route: Router,
    private activedroute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private navService: NavService,
    private localService: LocationService
    ) {
    this.localService.date.subscribe(this.dateChanged);

    this.linechartOptions = {
      series: [
        {
          name: "Power",
          data: this.lineData,
        }
      ],
      chart: {
        height: 350,
        type: "line",
        zoom: {
          enabled: false
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "straight"
      },
      title: {
        text: "Power",
        align: "left"
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      x_axis: {
        categories: [
          // "Jan",
          // "Feb",
          // "Mar",

        ]
      }
    };

    this.vbarchartOptions  = {
      series: [
        {
          data: [2.3, 3.1, 4.0]
        }
      ],
      chart: {
        height: 200,
        type: "bar"
      },
      plotOptions: {
        bar: {
          horizontal: false,
          dataLabels: {
            position: "top" // top, center, bottom
          }
        }
      },
      dataLabels: {
        enabled: true,
        formatter: function(val) {
          return val + "%";
        },
        offsetY: -20,
        style: {
          fontSize: "12px",
          colors: ["#304758"]
        }
      },

      x_axis: {
        categories: [
          "R",
          "T",
          "S",
        ],
        position: "top",
        labels: {
          offsetY: -18
        },
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        },
      },
      y_axis: {
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        },
        labels: {
          show: false,
          formatter: function(val) {
            return val + "%";
          }
        }
      },

    };

    this.hbarchartOptions = {
      series: [
        {
          name: "reversed",
          data: [400, 430, 448]
        }
      ],
      chart: {
        type: "bar",
        height: 200
      },

      plotOptions: {
        bar: {
          horizontal: true
        }
      },
      dataLabels: {
        enabled: false
      },
      x_axis: {
        categories: [
          "R",
          "S",
          "T",
          "Total"
        ]
      },
      grid: {
        xaxis: {
          lines: {
            show: true
          }
        }
      },
      y_axis: {
        reversed: false,
        axisTicks: {
          show: true
        }
      }
    };

    this.piechartOptions = {
      title: {
       text: "Power",
       align: "center"
      },
      series: [44, 55, 41],
      chart: {
        width: 300,
        type: "donut"
      },
      labels: [
        "kW",
        "kVA",
        "kvar",
      ],
      legend: {
        formatter: function(val, opts) {
          return val + " - " + opts.w.globals.series[opts.seriesIndex];
        }
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };
    this.query = new QueryService();
    this.influx = new InfluxdbService();
    this.GenMenuList();
    // this.fetchData(this.selectedDate);
  }

  ngOnInit(): void {

    // this.fetchData(this.selectedDate);
    this.route.routeReuseStrategy.shouldReuseRoute = function(){
            return false;
         };
    this.route.events.subscribe((evt) => {
            if (evt instanceof NavigationEnd) {

              // trick the Router into believing it's last link wasn't previously loaded
              this.route.navigated = false;
               // if you need to scroll back to top, here is the right place
              // window.scrollTo(0, 0);

            }
        });
    this.spinner.show(undefined, { fullScreen: true });
    setTimeout(() => {
      this.spinner.hide();
    }, 5000);
    // this.fetchData(this.selectedDate);
    // this.route.events.pipe(
    //   filter((event: RouterEvent) => event instanceof NavigationEnd)
    // ).subscribe(() => {
    //   this.fetchData(this.selectedDate);
    // });
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    // this.fetchData(this.selectedDate);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.navService.popTitle();
    this.navService.pushTitle("TferDash");
  }

  fetchData(date): void {
    // console.time('location settled-in');
    this.spinner.show(undefined, { fullScreen: true });
    setTimeout(() => {
      this.spinner.hide();
    }, 5000);
    const newLocal = +this.activedroute.snapshot.paramMap.get('id');
    let r = this.allPromise(newLocal - 1, date).then((result) => {
      console.log(result);
      // for (let x = 1; x < result.length; x++) {
      //   for (let y = 0; y < result[x].length; y++) {
      //       result[0][y] += result[x][y];
      //   }
      // }
      if(result[0].length > 0) {
        this.electricHourList = result[0][0];
        this.linechartOptions.series.pop();
        let d = { name:"Power", data: this.electricHourList.kw};
        this.linechartOptions.series = [d];

        const _kwarr = this.electricHourList.kw
        const kwAvg = _kwarr.reduce((a,b) => a + b, 0) / _kwarr.length

        const _kvaarr = this.electricHourList.kva
        const kvaAvg = _kvaarr.reduce((a,b) => a + b, 0) / _kvaarr.length

        const _kvararr = this.electricHourList.kvar
        const kvarAvg = _kvararr.reduce((a,b) => a + b, 0) / _kvararr.length

        this.piechartOptions.series.pop();
        this.piechartOptions.series = [kwAvg, kvaAvg, kvarAvg ];
      }
      if(result[1].length > 0){
        this.hbarchartOptions.series.pop();
        this.hbarchartOptions.series = [ { data: result[1] }];
      }
      if(result[2].length > 0){
        this.vbarchartOptions.series.pop();
        this.vbarchartOptions.series = [ { data: result[2] }];
      }
      if(result[3].length > 0){
        this.electric.P3 = result[3];
      }
      if(result[4].length > 0){
        this.electric.P2 = result[4];
      }
      if(result[5].length > 0){
        this.electric.A = result[5];
      }

    }).catch((err) => {
      console.log(err);
    })
  }

  selectedDate: Date = new Date();
  private GenMenuList() {
    const newLocal = +this.activedroute.snapshot.paramMap.get('id');
    this.localService.getLocal((newLocal >= 0) ? newLocal : 1).subscribe(location => this.selectedLocation = location);
    for (let i = 0; i < LOCATIONS.length; i++) {
      console.log(LOCATIONS[i]);
      let item: MenuSectionItem = {
        title: LOCATIONS[i].name,
        href: '/location/' + LOCATIONS[i].id,
      };
      this.menu.sections[0].items.push(item);
    }
    this.navService.resetTitle();
    this.navService.pushTitle(this.selectedLocation.name);
  }

  onDateChange(event) {
    console.log(event);
    this.selectedDate= event;
    this.fetchData(event);
  }

  showKW() {
    console.log("showKW click");
    this.linechartOptions.series.pop();
    let d = { name:"Power", data: this.electricHourList.kw};
    this.linechartOptions.series = [d];
  }

  showKVAR() {
    console.log("showKVAR click");
    this.linechartOptions.series.pop();
    let d = { name:"Power", data: this.electricHourList.kvar};
    this.linechartOptions.series = [d];
  }

  showKVA() {
    console.log("showKVA click");
    this.linechartOptions.series.pop();
    let d = { name:"Power", data: this.electricHourList.kva};
    this.linechartOptions.series = [d];
  }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
