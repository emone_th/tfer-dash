import { Component, ViewChild, OnInit } from '@angular/core';
import { ChartComponent } from "ng-apexcharts";
import {Local, LOCATIONS } from '../../services/location';
import { LocationService } from '../../services/location.service';
import {
  ApexNonAxisChartSeries,
  ApexAxisChartSeries,
  ApexTitleSubtitle,
  ApexResponsive,
  ApexChart,
  ApexFill,
  ApexDataLabels,
  ApexPlotOptions,
  ApexLegend,
  ApexXAxis,
  ApexYAxis,
  ApexStroke,
  ApexGrid,
  ApexTheme,
} from "ng-apexcharts";
import * as moment from 'moment';
import { InfluxdbService } from 'src/app/services/influxdb.service';
import { QueryService } from 'src/app/services/query.service';

export type ChartOptions = {
  series: ApexNonAxisChartSeries | ApexAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  fill: ApexFill;
  legend: ApexLegend;
  dataLabels: ApexDataLabels;
  colors: string[];
  plotOptions: ApexPlotOptions;
  title: ApexTitleSubtitle;
  x_axis: ApexXAxis;
  y_axis: ApexYAxis;
  grid: ApexGrid;
  stroke: ApexStroke;
  theme: ApexTheme;
};

@Component({
  selector: 'app-power',
  templateUrl: './power.component.html',
  styleUrls: ['./power.component.scss']
})
export class PowerComponent implements OnInit {

  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartsOptions: any[];

  private influx: InfluxdbService;
  private query: QueryService;

  dateChanged = (date) => {
    this.selectedDate = date;
    this.fetchData(date);
  }

  constructor(private localService: LocationService) {
    this.localService.date.subscribe(this.dateChanged);
    this.query = new QueryService();
    this.influx = new InfluxdbService();
    this.chartsOptions = [];

    for (let i = 0; i < LOCATIONS.length; i++)
    {
      this.chartsOptions.push( {
          title: {
            text: LOCATIONS[i].name
          },
          series: [44, 55, 13],
          chart: {
            type: "donut",
            height: "150px",
            width: "200px"
          },
          labels: [
            "kW",
            "kvar",
            "kVA"
          ],
          fill: {
            type: "gradient",
          },
          dataLabels: {
            enabled: true
          },
          legend: {
            formatter: function(val, opts) {
              return val + " - " + opts.w.globals.series[opts.seriesIndex];
            }
          },
          responsive: [
            {
              breakpoint: 480,
              options: {
                chart: {
                  width: "200"
                },
                legend: {
                  position: "bottom"
                }
              }
            }
          ]
        }     );
    }
  }


  getPowerPromise = (location, slave, date): Promise<any> => {
    return new Promise<any>(
      (resolve) => {
        try {
          let c = date;
          c.setHours(23,59,59);
          let e = c.toISOString();
          let s = moment(c).add(-1, 'day').toISOString();
          const q = `from(bucket: "tfer")
        |> range(start: time(v: "${s}"), stop: time(v: "${e}"))
        |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave.toString()}")
        |> filter(fn: (r) => r["_field"] == "pA" or r["_field"] == "pB" or r["_field"] == "pC")
        |> aggregateWindow(every: duration(v: "1d"), fn: mean, createEmpty: true)
        |> mean(column: "_value")`;
          this.influx.queryFluxString(q).then(data =>
            {
              let d = [];

                if (data.length > 0) {
                  data.forEach(element => {
                    if(element._value === 0)
                      d.push(1);
                    else
                      d.push(element._value);
                  });
                } else {
                  // let n = [null, null,null, null,null, null,null, null,null, null,null, null,
                  //   null, null,null, null,null, null,null, null,null, null,null, null,];
                  // d = [null,null,null];
                  // d = [0,0,0];
                  d = [1,1,1];
                }


              // if(data.length === 0)
              // {
              //   for (let index = 0; index < 3; index++) {
              //     d.push(0);
              //   }
              // }
              resolve(d);
            });
        } catch (error) {
          console.log(error);
        }
      }
    )
  }

  allPowerPromise(date) : any {
    const a = Promise.all<any>(
      [
        this.getPowerPromise("P1", 1, date),  // จุดที่ 1
        this.getPowerPromise("P2", 1, date),  // จุดที่ 2
        this.getPowerPromise("P3", 1, date),  // จุดที่ 3
        this.getPowerPromise("P5", 1, date),  // จุดที่ 4
        this.getPowerPromise("P5", 2, date),  // จุดที่ 5
        this.getPowerPromise("P4", 1, date),  // จุดที่ 6
        this.getPowerPromise("P4", 2, date),  // จุดที่ 7
        this.getPowerPromise("P6", 1, date),  // จุดที่ 8
        this.getPowerPromise("P7", 1, date),  // จุดที่ 9
        this.getPowerPromise("P7", 2, date),  // จุดที่ 10
        this.getPowerPromise("P7", 3, date),  // จุดที่ 11
        this.getPowerPromise("P8", 1, date),  // จุดที่ 12
        this.getPowerPromise("P8", 2, date),  // จุดที่ 13
        this.getPowerPromise("P8", 3, date),  // จุดที่ 14
      ]
    );
    return a;
  }

  ngOnInit(): void {
    this.fetchData(this.selectedDate);
  }

  fetchData(date) : void {
    this.allPowerPromise(date).then((result) =>
    {
      console.log(result);
      for (let i = 0; i < LOCATIONS.length; i++)
      {
        this.chartsOptions[i].series = result[i];
      }
    });
  }

  selectedDate: any;
  onDateChange(event) {
    console.log(event);
    this.selectedDate= event;
    this.fetchData(event);
  }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
