import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './components/main/main.component';
import { ConsumeComponent } from './components/consume/consume.component';
import { PowerComponent } from './components/power/power.component';
import { HarmonicComponent } from './components/harmonic/harmonic.component';
import { AlarmComponent } from './components/alarm/alarm.component';
import { SystemComponent } from './components/system/system.component';
import { FactorComponent } from './components/factor/factor.component';
import { ElectricComponent } from './components/electric/electric.component';
import { LocationComponent } from './components/location/location.component';

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: 'main', component: MainComponent },
  { path: 'consume', component: ConsumeComponent },
  { path: 'power', component: PowerComponent },
  { path: 'harmonic', component: HarmonicComponent },
  { path: 'alarm', component: AlarmComponent },
  { path: 'system', component: SystemComponent },
  { path: 'factor', component: FactorComponent },
  { path: 'electric', component: ElectricComponent },
  { path: 'location', redirectTo: '/location/1', pathMatch: 'full'  },
  { path: 'location/:id', component: LocationComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes
    ,{
      onSameUrlNavigation: 'reload',
      useHash: true
    }
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
