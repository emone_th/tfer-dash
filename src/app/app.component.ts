import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  OnInit,
  Output,
} from '@angular/core';
import {MatCalendar,MatDatepickerInputEvent} from '@angular/material/datepicker';
import {DateAdapter, MAT_DATE_FORMATS, MatDateFormats} from '@angular/material/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FormControl} from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { LocationService } from './services/location.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit ,OnChanges{

  public date = new FormControl(new Date());

  title = 'Smart factory dashboard';
  public response: any;

  @Output() onDateChange = new EventEmitter<any>();

  maxDate:Date;
  constructor(private route: Router,
    private activedroute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private localService: LocationService) {
      this.maxDate = new Date(Date.now());
      this.localService.date.next(this.date.value);
      // this.onDateChange.emit(this.date);
  }

  ngOnInit(){
    this.showSpinner();
  }

  ngOnChanges() {

  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    console.log(`${type}: ${event.value.toISOString()}`);
    if (type === 'change') {
      this.showSpinner();
      this.date.setValue(event.value);
      // this.onDateChange.emit(event.value);
      this.localService.date.next(event.value);
    }

    this.route.routeReuseStrategy.shouldReuseRoute = function(){
      return false;
    };
    this.route.events.subscribe((evt) => {
      // console.log(evt);
      if (evt instanceof NavigationEnd) {
        // this.fetchData();
        // trick the Router into believing it's last link wasn't previously loaded
        this.route.navigated = false;
         // if you need to scroll back to top, here is the right place
        // window.scrollTo(0, 0);

      }

    });
  }

  showSpinner() {
    this.spinner.show(undefined, { fullScreen: true });
    setTimeout(() => {
      this.spinner.hide();
    }, 5000);
  }

  changeOptions() {
    this.spinner.show(undefined,
      {
        type: 'square-spin',
        size: 'medium',
        bdColor: 'rgba(100,149,237, .8)',
        color: 'white',
        fullScreen: true
      }
    );
    setTimeout(() => this.spinner.hide(), 5000);
  }

  onActivate(event) {
    // console.log(event)
    event.onDateChange(this.date.value);
  }

  onSelectedValueChange(event) {
    // console.log(event)
    event.onDateChange(this.date.value);
  }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
