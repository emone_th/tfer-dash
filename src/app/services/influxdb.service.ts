import { Injectable } from '@angular/core';
import {
  InfluxDB,
  ParameterizedQuery,
  QueryApi
} from "@influxdata/influxdb-client";

// import { OrgsAPI, QueryAPI } from "@influxdata/influxdb-client-apis";
import { url, org, bucket, token } from './env'
// import {from} from 'rxjs'
// import {map} from 'rxjs/operators'
import { LOCATIONS } from './location'

@Injectable({
  providedIn: 'root'
})
export class InfluxdbService {
  private db: InfluxDB;
  public queryApi: QueryApi;

  constructor() {
    this.db = new InfluxDB({url, token});
    this.queryApi = this.db.getQueryApi(org);
  }

  async queryFluxString(
    fluxQuery: string | ParameterizedQuery
    ) : Promise<any> {
      // let retRows = [];
      return this.queryApi.collectRows(fluxQuery,  /*, you can specify a row mapper as a second arg */)
          .then(data => {
            // data.forEach(x => {
            //   retRows.push(x);
            // })
            console.log(fluxQuery);
            console.log(data);
            console.log('\nCollect ROWS SUCCESS')
            return data;
          })
          .catch(error => {
            console.error(error)
            console.log('\nCollect ROWS ERROR')
          });

//  Execute query and collect all results in a Promise.
//  Use with caution, it copies the whole stream of results into memory.

  }

  // queryRXJS(fluxQuery: string | ParameterizedQuery) : any {
  //   let retRows = [];
  //   let queryAPI = this.db.getQueryApi(org);
  //   from (queryAPI.rows(fluxQuery))
  //     .pipe(map(({values, tableMeta}) => tableMeta.toObject(values)))
  //     .subscribe({
  //       next(o) {
  //         console.log(
  //           `location: ${o.location} slave id: ${o.slave_id} => ${o._field}=${o._value}`
  //         );
  //       },
  //       error(e) {
  //         console.error(e)
  //         console.log('\nFinished ERROR')
  //       },
  //       complete() {
  //         console.log('\nFinished SUCCESS')
  //       },
  //     });
  //   return retRows;
  // }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
