import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, Subject } from 'rxjs';

import { catchError, map, tap } from 'rxjs/operators';

import { Local, LOCATIONS } from './location';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  date = new Subject<Date>();

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(

  ) { }

  getLocals(): Observable<Local[]> {
    return of(LOCATIONS);
  }

  getLocal(id: number): Observable<Local> {
    return of(LOCATIONS.find(location => location.id === id));
  }
}

/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
