import { Time } from '@angular/common';

export const locationList = [ "P1", "P2", "P3", "P5", "P5", "P4", "P4", "P6", "P7", "P7", "P7", "P8", "P8", "P8"];
export const slaveList = [1,1,1,1,2,1,2,1,1,2,3,1,2,3];

export interface propertiesElectric {
  current : {
    A: number;
    B: number;
    C: number;
    N: number;
    G: number;
    Average: number;
    unbalancA: number;
    unbalancB: number;
    unbalancC: number;
    unbalancWorst: number;
  },
  voltage : {
    AB: number;
    voltageBC: number;
    voltageCA: number;
    voltageLLAvg: number;
    voltageAL: number;
    voltageBL: number;
    voltageCL: number;
    voltageLNAvg: number;
  },
  power : {
    activeA: number,
    activeB: number,
    activeC: number,
    activeTotal: number,
    FactorA: number,
    FactorB: number,
    FactorC: number,
    FactorTotal: number,
  },
  demandChannel : {
    systemAssignment: number,
    registerNumberofMeterQuanities: number,
    unitCode: number,
    lastDemand: number,
    presentDemand: number,
    predictedDemand: number,
    peakDemand: number,
    peakDatetime: {
      date: Date,
      time: Time
    }
  }
}

export interface Local {
  id: number;
  name: string;
  values?: Array<propertiesElectric>;
}

export interface Electric {
  P3: Array<number>;
  P2: Array<number>;
  A: Array<number>;
}

export const LOCATIONS: Local[] = [
  { id: 1, name: 'จุดที่ 1 Office, โรงอาหาร, ลานจอดรถ'},
  { id: 2, name: 'จุดที่ 2 อาคารวิศวกรรม, คลังวัสดุ' },
  { id: 3, name: 'จุดที่ 3 สถานีบรรจุ, ที่พักผู้รับเหมา PK, Bulk storage' },
  { id: 4, name: 'จุดที่ 4 Plant 1 (เครื่องจักรหนัก)' },
  { id: 5, name: 'จุดที่ 5 Plant 1 (เครื่องจักรเบา)' },
  { id: 6, name: 'จุดที่ 6 สาธารณูปโภค Utility' },
  { id: 7, name: 'จุดที่ 7 คลังเก็บวัตถุดิบ' },
  { id: 8, name: 'จุดที่ 8 Truck scale' },
  { id: 9, name: 'จุดที่ 9  Bulk blend' },
  { id: 10, name: 'จุดที่ 10 Single line' },
  { id: 11, name: 'จุดที่ 11 Ware house P16-P19' },
  { id: 12, name: 'จุดที่ 12 สถานีหน้าท่า Auto jetty' },
  { id: 13, name: 'จุดที่ 13 สถานีหน้าท่า ลานบรรจุ Office AJ' },
  { id: 14, name: 'จุดที่ 14 ที่พักลูกค้า' }
];


/*
Copyright EmOne. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
