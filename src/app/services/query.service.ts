import { Injectable } from '@angular/core';
import {
  FluxTableMetaData,
  flux,
  fluxDuration,
  fluxDateTime,
  sanitizeFloat,
  ParameterizedQuery,
} from "@influxdata/influxdb-client";
export interface Local {
  id: string;
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class QueryService {

  constructor() { }

  public getPowerOverAll : string =
    `from(bucket: "tfer")
    |> range(start: 2020-09-07T11:41:24Z, stop: 2020-10-10T11:41:24Z)
    |> filter(fn: (r) => r["_measurement"] == "tfer_pm")
    // |> filter(fn: (r) => r["location"] == "P1")
    // |> filter(fn: (r) => r["slave_id"] == "1")
    |> aggregateWindow(every: 1d, fn: mean, createEmpty: false)
    |> yield(name: "mean")`;

  ParamStringMean(bucket: string, measurement: string, duration: string,
      time_start: string, time_stop: string, location: string, slave: string,
      fields: string[], value: string, t: string) : any {
      let dauer = fluxDuration(duration);
      let start = fluxDateTime(time_start);
      let stop = fluxDateTime(time_stop);

      let r = [];
      for (const f of fields) {
        r.push(`r["_field"] == "${f}"`)
      }

      let fluxQuery = `from(bucket: "${bucket}")
        |> range(start: ${start}, stop: ${stop})
        |> filter(fn: (r) => r["_measurement"] == "${measurement}")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave}")
        |> filter(fn: (r) => ${r.join(" or ")})
        |> aggregateWindow(every: ${dauer}, fn: ${value}, createEmpty: ${t})
        |> mean(column: "_value")`;

      return fluxQuery.toString();
    }

    ParamString(bucket: string, measurement: string, duration: string,
      time_start: string, time_stop: string, location: string, slave: string,
      fields: string[], value: string, t: string) : any {
      let dauer = fluxDuration(duration);
      let start = fluxDateTime(time_start);
      let stop = fluxDateTime(time_stop);

      let r = [];
      for (const f of fields) {
        r.push(`r["_field"] == "${f}"`)
      }

      let fluxQuery = `from(bucket: "${bucket}")
        |> range(start: ${start}, stop: ${stop})
        |> filter(fn: (r) => r["_measurement"] == "${measurement}")
        |> filter(fn: (r) => r["location"] == "${location}")
        |> filter(fn: (r) => r["slave_id"] == "${slave}")
        |> filter(fn: (r) => ${r.join(" or ")})
        |> aggregateWindow(every: ${dauer}, fn: ${value}, createEmpty: ${t})`;

      return fluxQuery.toString();
    }

}
